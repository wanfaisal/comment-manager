import React from 'react';
import { Navbar } from "react-bootstrap";
import Container from 'react-bootstrap/Container';

const Header = () => {
  return (
    <Navbar bg="dark">
        <Container>
          <Navbar.Brand style={{ color: 'white'}}>Comment Manager</Navbar.Brand>
        </Container>
    </Navbar>
  );
}

export default Header;
import React from "react";
import {
    createBrowserRouter
} from "react-router-dom";


import PostList from "./pages/postlist";
import PostDetail from "./pages/post";

const router = createBrowserRouter([
    {
        path: "/",
        element: <PostList />,
    },
    {
        path: "/post/:id",
        element: <PostDetail />,
    }
]);

export default router;
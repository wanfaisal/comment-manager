import './App.css';
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import { useParams } from "react-router-dom";

const App = () => {
    let { id } = useParams();
  
    const [searchTerm, setSearchTerm] = useState('');
    const [dataPost, setDataPost] = useState(null);
    const [dataComment, setDataComment] = useState(null);
    const [errorPost, setErrorPost] = useState(null);
    const [errorComment, setErrorComment] = useState(null);

    useEffect(() => {
      axios.get('https://jsonplaceholder.typicode.com/posts/' + id)
      .then(res => {
        setDataPost(res.data)
      })
      .catch(error => setErrorPost(error));

      axios.get(' https://jsonplaceholder.typicode.com/comments?postId=' + id)
      .then(res => {
        let commentData = []
        for (let i = 0; i < res.data.length; i++) {
          res.data[i].string = res.data[i].name + " " + res.data[i].email + " " + res.data[i].body
          commentData.push(res.data[i])
        }
        setDataComment(commentData)
      })
      .catch(error => setErrorComment(error));
    });

    if (errorPost) {
      return <div>An error occurred: {errorPost.message}</div>;
    }

    if (errorComment) {
      return <div>An error occurred: {errorComment.message}</div>;
    }
  
    if (!dataPost  || !dataComment) {
      return <div>Loading...</div>;
    }

    const filteredData = dataComment.filter(item =>
      item.string.toLowerCase().includes(searchTerm.toLowerCase())
    );
  
    return (
      <div style={{ paddingLeft: '10px', paddingRight: '10px'}}>
        <div style={{ 
          display: 'flex', 
          justifyContent: 'start', 
          alignItems: 'center', 
          height: '10vh',
          marginLeft: '5px' }}>
          <input
            placeholder='Search..'
            type="text"
            value={searchTerm}
            onChange={e => setSearchTerm(e.target.value)}
          />
        </div>
        <Card style={{ marginTop: '10px', textAlign: 'justify'}}>
          <Card.Body style={{ fontWeight: 'bold'}}>
            <Card.Text>
              {dataPost.body}
            </Card.Text>
          </Card.Body>
        </Card>
        {filteredData.map(item => (
          <Card key={item.id} style={{ marginTop: '10px', textAlign: 'justify'}}>
            <Card.Body >
              <Card.Text>
                <div>
                  <div>
                    <span style={{ fontWeight: 'bold'}}>Name: </span>
                    <span>{item.name}</span>
                  </div>
                  <div>
                  <span style={{ fontWeight: 'bold'}}>Email: </span>
                  <span>{item.email}</span>
                  </div>
                </div>
                <div style={{ 
                  backgroundColor: 'peachpuff', 
                  margin: '10px',
                  padding: '10px'
                }}>
                  {item.body}
                </div>
              </Card.Text>
            </Card.Body>
          </Card>
        ))}
      </div>
    );
}

export default App;

import './App.css';
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import { BsBoxArrowInUpRight } from "react-icons/bs";
import { Outlet, Link } from "react-router-dom";

const CardItem = ({ data }) => {
    return (
        <Card style={{ marginTop: '10px', textAlign: 'justify'}}>
          <Card.Body>
            <Card.Title>
                <Link to={`post/` + data.id}>
                {data.title} <span> </span>
                <BsBoxArrowInUpRight size={20} color="#333" />
                </Link>
            </Card.Title>
            <Card.Text>{data.body}</Card.Text>
          </Card.Body>
        </Card>

    );
}

const App = () => {

    const [data, setData] = useState(null);
    const [error, setError] = useState(null);

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/posts')
          .then(res => {
            setData(res.data)
          })
          .catch(error => setError(error));
    }, []);

    if (error) {
        return <div>An error occurred: {error.message}</div>;
    }

    if (!data) {
        return <div>Loading...</div>;
    }

    return (
        <Card body>
        <div>
          {data.map(item => (
            <CardItem
              key={item.id} 
              data={item}
            />
          ))}
        </div>
        </Card>
    );
}

export default App;
